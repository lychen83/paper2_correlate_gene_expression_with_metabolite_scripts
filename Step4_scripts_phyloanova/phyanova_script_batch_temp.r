library("geiger")
library('ape')
setwd("/home/clingyun/CHEN/paper2/correlation_phylogeny_R/analysis_feb2019/outdir_tpm_feb2019/")
tree = read.nexus("SpeciesTree_19974genes_54ingroup.tree", tree.names = NULL, force.multi = FALSE)
samples = read.table("Bv2_029930_ucyh.raxml.tre.1.inclade",row.names=1)
x = samples[,1]
names(x) = rownames(samples)
f = samples[,2]
names(f) = rownames(samples)
f = as.factor(f)
aov.phylo(x~f,tree,nsim=100)