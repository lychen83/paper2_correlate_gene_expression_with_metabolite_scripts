'''
This script was used to run batch phylolm
'''

import sys,re,os,commands,ntpath

def get_filename_from_path(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python batch_phyanova.py indir_quant_trait example_phyanova_script outDIR"
		sys.exit(0)

	inDIR = sys.argv[1]+"/"
	script = sys.argv[2]
	outDIR = sys.argv[3]+"/"

	num_quat_trait_file = 0
	num_passed = 0
	for quant_trait in os.listdir(inDIR):
		if quant_trait.startswith('Bv'):
			num_quat_trait_file += 1
			print 'processing quant_trait for gene', quant_trait, num_quat_trait_file, 'genes processed'
			file_name = get_filename_from_path(quant_trait)
			with open(script,"r") as infile:
				cmd = ['rm', script]
				cmd = " ".join(cmd)
				os.system(cmd)
				for line in infile: 
					if 'samples = read.table' in line: 
						line = 'samples = read.table("'+ file_name + '",row.names=1)\n'
					with open(script,"a") as outfile:
						outfile.write(line)
			cmd = ['/home/clingyun/CHEN/biosofts/R-3.4.4_atlas_installed/lib64/R/bin/Rscript', script]
			cmd = " ".join(cmd)
			result = commands.getoutput(cmd)  # Read the output of phylolm
			#print result
			if 'group' in result:
				result = result.split('\n')
				result = result[5].split(' ') 
				for xx in result:
					if xx == '':
						result.remove(xx)
				print result
				if float(result[5]) <= 0.01:    #result[5] is the p value
					num_passed += 1
					print quant_trait, 'passed. ', 'In total', num_passed, 'gene passed\n\n', ''
					with open(outDIR+'gene_passed_phyanova',"a") as outfile:
						outfile.write(str(quant_trait) +"\n")
						outfile.write(str(result) +"\n")
						dir = inDIR + quant_trait
						cmd = ["cp", dir, outDIR]
						cmd = " ".join(cmd)
						os.system(cmd)