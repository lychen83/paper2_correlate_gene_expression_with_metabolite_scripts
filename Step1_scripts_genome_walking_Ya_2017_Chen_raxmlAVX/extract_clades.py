"""
Extract ingroup clades when outgroups present

Prepare a taxon code file with each line look like (separated by tab):
IN	taxonID1
IN	taxonID2
OUT	taxonID3
"""

import phylo3,newick3,os,sys,ntpath

#if pattern changes, change it here
#given tip label, return taxon name identifier
def get_name(label):
	return label.split("@")[0]

def get_front_labels(node):
	leaves = node.leaves()
	return [i.label for i in leaves]

def get_back_labels(node,root):
	all_labels = get_front_labels(root)
	front_labels = get_front_labels(node)
	return set(all_labels) - set(front_labels) #labels do not repeat
	
def get_front_names(node): #may include duplicates
	labels = get_front_labels(node)
	return [get_name(i) for i in labels]

def get_back_names(node,root): #may include duplicates
	back_labels = get_back_labels(node,root)
	return [get_name(i) for i in back_labels]

def get_filename_from_path(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)
	
def remove_kink(node,curroot):
	if node == curroot and curroot.nchildren == 2:
		#move the root away to an adjacent none-tip
		if curroot.children[0].istip: #the other child is not tip
			curroot = phylo3.reroot(curroot,curroot.children[1])
		else: curroot = phylo3.reroot(curroot,curroot.children[0])
	#---node---< all nodes should have one child only now
	length = node.length + (node.children[0]).length
	par = node.parent
	kink = node
	node = node.children[0]
	#parent--kink---node<
	par.remove_child(kink)
	par.add_child(node)
	node.length = length
	return node,curroot
	
#input is a tree with both ingroups and more than 1 outgroups
def extract_ingroup_clades(root):
	inclades = []
	while True:
		max_score,direction,max_node = 0,"",None
		for node in root.iternodes():
			front,back = 0,0
			front_names_set = set(get_front_names(node))
			for name in front_names_set:
				if name in OUTGROUPS:
					front = -1
					break
				else: front += 1
			back_names_set = set(get_back_names(node,root))
			for name in back_names_set:
				if name in OUTGROUPS:
					back = -1
					break
				else: back += 1
			if front > max_score:
				max_score,direction,max_node = front,"front",node
			if back > max_score:
				max_score,direction,max_node = back,"back",node
		#print max_score,direction
		if max_score >= MIN_INGROUP_TAXA:
			if direction == "front":
				inclades.append(max_node)
				kink = max_node.prune()
				if len(root.leaves()) > 3:
					newnode,root = remove_kink(kink,root)
				else: break
			elif direction == "back":
				par = max_node.parent
				par.remove_child(max_node)
				max_node.prune()
				inclades.append(phylo3.reroot(root,par))#flip dirction
				if len(max_node.leaves()) > 3:
					max_node,root = remove_kink(max_node,max_node)
				else: break
		else: break
	return inclades

if __name__ == "__main__":
	if len(sys.argv) != 7:
		print "python extract_clades.py inDIR treefileending outDIR MIN_INGROUP_TAXA TAXON_CODE output_name"
		sys.exit(0)
	
	inDIR = sys.argv[1]+"/"
	treefileending = sys.argv[2]
	outDIR = sys.argv[3]+"/"
	MIN_INGROUP_TAXA = int(sys.argv[4])
	taxon_code_file = sys.argv[5]
	outname = sys.argv[6]

	INGROUPS = []
	OUTGROUPS = []
	with open(taxon_code_file,"r") as infile:
		for line in infile:
			if len(line) < 3: continue
			spls = line.strip().split("\t")
			if spls[0] == "IN":
				INGROUPS.append(spls[1])
			elif spls[0] == "OUT":
				OUTGROUPS.append(spls[1])
			else:
				print "Check TAXON_CODE file format"
				sys.exit()
	if len(set(INGROUPS) & set(OUTGROUPS)) > 0:
		print "Taxon ID",set(INGROUPS) & set(OUTGROUPS),"in both ingroups and outgroups"
		sys.exit(0)
	print len(INGROUPS),"ingroup taxa and",len(OUTGROUPS),"outgroup taxa read"
	print "Ingroups:",INGROUPS
	print "Outgroups:",OUTGROUPS
	
	for treefile in os.listdir(inDIR):
		if treefile[-len(treefileending):] != treefileending: continue
		with open(inDIR+treefile,"r") as infile:
			 intree = newick3.parse(infile.readline())
		curroot = intree
		all_names = get_front_names(curroot)
		print treefile
		
		bait_gene_name = get_filename_from_path(treefile).split(".")[0]
		if bait_gene_name.endswith("_1"): bait_gene_name.replace("_1", "")
		
		#check taxonIDs
		ingroup_names = []
		outgroup_names = []
		for name in all_names:
			if name in INGROUPS:
				ingroup_names.append(name)
			elif name in OUTGROUPS:
				outgroup_names.append(name)
			else:
				print name,"not in ingroups or outgroups"
				sys.exit()
		if len(set(ingroup_names)) < MIN_INGROUP_TAXA:
			print "not enough ingroup taxa in tree"
			continue
		if len(outgroup_names) == 0:
			print "No outgroup in tree"
			continue

		inclades = extract_ingroup_clades(curroot)
		inclade_count = 0
		for inclade in inclades:
			labels = get_front_labels(inclade)
			for label in labels: 
				if bait_gene_name not in label: continue
				inclade_count += 1
				inclade_name = outDIR+treefile+"."+str(inclade_count)+"."+outname
				with open(inclade_name,"w") as outfile:
					outfile.write(newick3.tostring(inclade)+";\n")
		print inclade_count,"clades extracted"
