library('ape')
library('phylolm')

setwd("/home/clingyun/CHEN/paper2/correlation_phylogeny_R/analysis_feb2019/outdir_tpm_feb2019/")
getwd()

samples = read.table("Bv2_040980_izsm.raxml.tre.2.inclade",row.names=1)
tree = read.nexus("SpeciesTree_19974genes_54ingroup.tree", tree.names = NULL, force.multi = FALSE)
	
fit = phyloglm(formula=pigment~tpm, data=samples, phy=tree, method = ("logistic_MPLE"), boot = 0)
summary(fit)
coef(fit)
vcov(fit)
