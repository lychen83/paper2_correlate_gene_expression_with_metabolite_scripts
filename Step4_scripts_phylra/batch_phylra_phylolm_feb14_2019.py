'''
This script was used to run batch phylolm
'''

import sys,re,os,commands,ntpath

def get_filename_from_path(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python batch_phylolm.py indir_quant_trait example_phylolm_script outDIR"
		sys.exit(0)

	inDIR = sys.argv[1]+"/"
	script = sys.argv[2]
	outDIR = sys.argv[3]+"/"

	num_quat_trait_file = 0
	num_passed = 0
	for quant_trait in os.listdir(inDIR):
		if quant_trait.startswith('Bv'):
			num_quat_trait_file += 1
			print 'processing quant_trait for gene', quant_trait, num_quat_trait_file, 'genes processed'
			file_name = get_filename_from_path(quant_trait)
			with open(script,"r") as infile:
				os.system('rm phylolm_script_batch_temp.r')
				for line in infile: 
					if 'samples = read.table' in line: 
						line = 'samples = read.table("'+ file_name + '",row.names=1)\n'
					with open('phylolm_script_batch_temp.r',"a") as outfile:
						outfile.write(line)
			cmd = '/home/clingyun/CHEN/biosofts/R-3.4.4_atlas_installed/lib64/R/bin/Rscript phylolm_script_batch_temp.r'
			result = commands.getoutput(cmd)  # Read the output of phylolm
			#print result
			if 'Signif. codes:' in result:  # if Signif. codes: in the phylra result, the different is significant
				num_passed += 1
				print quant_trait, 'passed. ', 'In total', num_passed, 'gene passed\n\n', ''
				print result
				with open(outDIR+'gene_passed_phylolm',"a") as outfile:
					outfile.write(str(quant_trait) +"\n")
					outfile.write(str(result) +"\n")
					dir = inDIR + quant_trait
					cmd = ["cp", dir, outDIR]
					cmd = " ".join(cmd)
					os.system(cmd)