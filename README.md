Most phylogenetic analyses and expression quantification were performed at the Mesabi compute cluster in Minnesota Supercomputing Institute (https://www.msi.umn.edu/content/mesabi). Other analyses were performed using a workstation with 24 cores and 96 Gb RAM in Ya Yang’s lab, University of Minnesota Twin Cities.

###Step 1: Phylogenetic analysis
Transcriptomes include 14 ancestral anthocyanin producing species from 11 families, 24 betalain species from 13 families, 16 species regained anthocyanin from three families, and ten outgroup species. 

1.1, to find out the homologous genes among species, protein sequences of Beta vulgaris genome (http://bvseq.boku.ac.at/Genome/Download/index.shtml; RefBeet-1.2) were used to identify homologous genes among all species and construct gene trees using scripts from Lopez-Nieves et al. (2017). Specifically, each protein sequence was used as ‘bait gene’ to search against other species separately with SWIPE v.2.0.12 (Rognes, 2011). A maximum of eight target sequences each species per query gene was set. Next, homologous genes were aligned using MAFFT v.7.130 with parameter ‘--genafpair --maxiterate 1000’. Aligned sites with missing ≥ 20% data were trimmed by Phyutility v.2.2.6. Gene tree was constructed using RAxML v.8.2.11 (Stamatakis et al., 2016) with model ‘PROTCATAUTO’. 

1.2, generate species tree using all gene trees with software STAG:
```
python stag.py taxon_table <folder_include_all_gene_trees>
```
taxon_table is as following:
```
MJM3333* MJM3333
NepSFB* NepSFB
NXTS* NXTS
Osativa* Osativa
```
MJM3333 is the species abbreviation in the gene tree

1.3, root the species tree using species Amborella_trichopoda.

1.4, Gene tree correction
At the MSI, conducting maximum likelihood (ML) analysis using RAxML with bootstrap replicates is very consuming. Therefore, we performed the ML analysis for anthocyanin or betalain related genes using RAxML-HPC v.8 on XSEDE via the CIPRES Science Gateway (https://www.phylo.org/portal2/home.action). 'PROTCAT' model and rapid bootstrap analysis with 100 replicates were used.

The gene trees were corrected by software profileNJ (Noutahi et al. 2016). profileNJ corrects gene tree by contracting weak branches and resolving them to have binary trees with a minimum reconciliation cost to their species tree. 
```
profileNJ -s <species_tree> -g <gene_tree> --sep '@' --spos 'prefix' -r best --seuil 85 -o <output_file_name>
```

1.5, Check the corrected gene tree, and manually added the words such as 'dup1_mark', 'dup2_mark', or 'dupnoncore_mark' to mark the position of the alpha, beta, gamma sub-clade.

###Step 2: Expression quantification
Salmon was used to map reads to transcriptomes. '--validateMappings' was used to improve both the sensitivity and specificity of mapping. For example,
```
salmon index -t MJM2667.cds.fa -i MJM2667.cds.fa.salmon.index
```
```
salmon quant -p 4 -i MJM2667.cds.fa.salmon.index --libType IU --dumpEq --validateMappings -1 SRR6435318_1.cor.p.fq.gz -2 SRR6435318_2.cor.p.fq.gz --output SRR6435318_salmon_map
```

Next, rename the quant.sf with species name, so that we did not messed up all quant.sf files:
```
mv XXXX_salmon_map/quant.sf XXXX_salmon_map_quant.sf
```
XXXX is species_name

###Step 3: Extract expression
3.1, Mark duplication in the extracted clade using characters such as 'dupalpha_mark', 'dupbeta_mark', or 'dupnoncore_mark'. For example, an extracted clade as following:
```
(((Species-A-1, Species-B-1), Species-A-2)dupalpha_mark, (Species-A-3, Species-B-2)dupbeta_mark);
```
'Species-A, Species-B, Species-C' is the species ID, '-1,-2,-3' is the transcript ID

3.2, extract expression level 
Run following command:
```
python extract_expression_phylogeny_duplication.py <folder_include_gene_expression_files(*.quant.sf)> <file_indicate_trait_for_species>  <folder_include_corrected_gene_trees> <output_folder>
```
For the tree in section 3.1, expression level for transcript Species-A-1 and Species-A-2 will be summed, and the summed value represents the expression level of Species-A in alpha clade.



---------------------------Following analyses may not be used in future, please ignore them
###Step 4: Generate data matrix for statistic analyses
Run following command:
```
python extract_expression_phylogeny_feb2019.py <folder_include_gene_expression_files(*.quant.sf)> <file_indicate_trait_for_species> <folder_include_gene_trees> <output_folder>
```
The analysis will generate a matrix for each gene, for example Bv9_226140_myxh:
```
tpm	pigment
blue_Ancistrocladaceae_Ancistrocladus_robertsoniorum	-3.32192809489	0
blue_Caryophyllaceae_Arenaria_serpyllifolia	1.42802604856	0
red_Phytolaccaceae_Phytolacca_americana	4.79773217403	1
.........
```
0 (blue) represents anthocyanin producing species. 1 (red) represents betalain producing species.

###Step 5: Run statistic analyses
4.1, Phylogenetic anova analysis and phylogenetic logistic regression analysis

First, we figured out the genes with average expression highger in red species (excluding missing species). We only kept genes with expression higher in betalain producing species, and missed red species <2. We only kept genes with expression higher in anthocyanin species, and missed anthocyanin species <5.

```
python compare_expression_value.py <folder_include_expression_file_from_last_step> <outDir_betalain_higher_species> <outDir_anthocyanin_higher_species>
```

Phylogenetic anova analysis 
We run the analysis using R package geiger to find out the genes with expression significantly higher (p <=0.01) in betalain species than that in anthocyanin species. The species tree and r script must be in the same folder. Make sure the path in the R script was changed to where the data matrix located (setwd("path_to_expression_located"))
```
python batch_phyanova_geiger_feb13_2019.py <folder_include_expression_file_higher_in_red> <an_r_script_phyloanova> <outdir>
```

Using similar command to run phylogenetic anova analysis for genes with expression higher in anthocyanin species.

Phylogenetic logistic regression(plr)analysis 
We run the analysis using R packge using R package phylolm to find out the genes that are correlated to betalain or anthocyanin.
```
python <batch_phylra_phylolm_feb14_2019.py> <folder_include_expression_file_higher_in_red> <an_r_script_phylra outdir>
```
Using similar command to run plr for genes with expression higher in anthocyanin species.

