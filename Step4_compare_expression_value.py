"""compare the average expression value of red and blue species, and output the genes with expression higher in red species and blue species separately"""

import sys,os,math,ntpath
import subprocess

def get_filename_from_path(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python compare_expression_value.py indir_include_expression_each_species outDIR_red_greater outDIR_blue_greater"
		sys.exit(0)
		
	inDIR = sys.argv[1]+"/"
	outDIR_red_greater = sys.argv[2]+"/"
	outDIR_blue_greater = sys.argv[3]+"/"
	number_red_greater = 0
	number_blue_greater = 0
	for file in os.listdir(inDIR):
		if file.startswith('Bv'):
			number_missing_red = 0
			number_missing_blue = 0
			red_value_list = []
			blue_value_list = []
			red_value_list_exclude_missing = []
			blue_value_list_exclude_missing = []
			name = get_filename_from_path(file)
			with open(inDIR + file,"r") as infile:
				for line in infile:
					line = line.strip()
					line = line.split('\t')
					if 'pigment' in line: continue # ignore the first line
					if float(line[2]) == 1.0: 
						red_value_list.append(float(line[1]))
					if float(line[2]) == 0.0 :
						blue_value_list.append(float(line[1]))
				
				for xx in red_value_list:
					if xx != -3.32192809489:
						red_value_list_exclude_missing.append(xx)
				for yy in blue_value_list:
					if yy != -3.32192809489:
						blue_value_list_exclude_missing.append(yy)
				if len(red_value_list_exclude_missing) > 0 and len(blue_value_list_exclude_missing) > 0:
					if sum(red_value_list_exclude_missing)/(len(red_value_list_exclude_missing)) >sum(blue_value_list_exclude_missing)/(len(blue_value_list_exclude_missing)) and sum(red_value_list_exclude_missing)/(len(red_value_list_exclude_missing)) >1.5:
						for aa in red_value_list:
							if aa == -3.32192809489:  # when expression 0.1 was log2 transformed, it is equal to -3.32192809489
								number_missing_red += 1
						for aa in blue_value_list:
							if aa == -3.32192809489:
								number_missing_blue += 1
						if number_missing_red < 2:  #We don't want to there are too many missing species
							number_red_greater +=1
							dir = inDIR + name
							cmd = ["cp", dir, outDIR_red_greater]
							cmd = " ".join(cmd)
							os.system(cmd)
					if sum(red_value_list_exclude_missing)/(len(red_value_list_exclude_missing)) <sum(blue_value_list_exclude_missing)/(len(blue_value_list_exclude_missing)) and sum(blue_value_list_exclude_missing)/(len(blue_value_list_exclude_missing)) >1.5:
						for aa in red_value_list:
							if aa == -3.32192809489:  # when expression 0.1 was log2 transformed, it is equal to -3.32192809489
								number_missing_red += 1
						for aa in blue_value_list:
							if aa == -3.32192809489:
								number_missing_blue += 1
						if number_missing_blue < 2: #We don't want to there are too many missing species
							number_blue_greater += 1
							dir = inDIR + name
							cmd = ["cp", dir, outDIR_blue_greater]
							cmd = " ".join(cmd)
							os.system(cmd)
				
	print "number_red_greater", number_red_greater, '\n'
	print "number_blue_greater", number_blue_greater, "\n"